//
//  Log.swift
//  SweptPoker
//
//  Created by Joel Glanfield on 2018-06-28.
//  Copyright © 2018 Joel Glanfield. All rights reserved.
//

import Foundation

func log(_ message: String) {
    print("\(message)")
}
