//
//  PokerHandEvaluator.swift
//  SweptPoker
//
//  Created by Joel Glanfield on 2018-07-03.
//  Copyright © 2018 Joel Glanfield. All rights reserved.
//

import Foundation

final class PokerHandEvaluator {
    
    private let hand1: Hand
    private let hand2: Hand
    
    // Each classification is made up of two functions:
    // 1. an evaluation function that tells us what we're looking for (e.g. four-of-a-kind, full-house, etc.)
    // 2. a tie-breaker function in the case that both hands satisfy the current evaluation (e.g. AAA22 > KKKQQ)
    //
    // Note 1: Classifications must be specified in priority order (i.e. four-of-a-kind > full-house
    //
    // Note 2: Certainly, we could create a concrete subclass for each classification-type, but I prefer
    // to use a function tuple to demonstrate some of the power of Swift, and to group classifications by priority for readability.
    private lazy var classifications: [(evaluator: ((Hand) -> Bool), tieBreaker: (() -> Hand?), classificationDescription: String)] = {
        return [(hasFourOfAKind, getBestMatchHand, "four-of-a-kind"),
         (hasFullHouse, getBestFullHouse, "full house"),
         (hasStraight, getBestStraight, "straight"),
         (hasThreeOfAKind, getBestMatchHand, "three-of-a-kind"),
         (hasTwoPair, getBestMatchHand, "two-pair"),
         (hasPair, getBestMatchHand, "pair")]
    }()
    
    init(hand1: Hand, hand2: Hand) {
        self.hand1 = hand1
        self.hand2 = hand2
    }
    
    func evaluate() -> (hand: Hand, winnerDescription: String, loserDescription: String)? {
        var results: (Hand, String, String)?
        
        // here we iterate through all possible classifications, and stop once we've found a winner        
        for classifier in classifications {
            if let winner = findBestHand(handEvaluator: classifier.evaluator, tieBreaker: classifier.tieBreaker) {
                let loser = winner == hand1 ? hand2 : hand1
                results = (winner, classifier.classificationDescription, getClassification(hand: loser))
                break
            }
        }
        
        // if we don't have a winner at this point, just compare highest cards
        if results == nil {
            if let highHand = getHandWithHighestCard() {
                return (highHand, "high-card", "high-card")
            }
        }
        
        return results
    }
    
    private func findBestHand(handEvaluator: (_ hand: Hand) -> Bool, tieBreaker: () -> Hand?) -> Hand? {
        let hand1Classified = handEvaluator(hand1)
        let hand2Classified = handEvaluator(hand2)
        
        if hand1Classified && hand2Classified {
            return tieBreaker()
        }
        
        return hand1Classified ? hand1 : (hand2Classified ? hand2 : nil)
    }
    
    private func getClassification(hand: Hand) -> String {
        if let description = (classifications.filter { $0.evaluator(hand) == true }.first?.classificationDescription) {
            return description
        }
        
        return "high-card"
    }
    
    private func hasMatch(hand: Hand, matchSize: Int) -> Bool {
        var count = 0
        for card in hand {
            count = 0
            hand.forEach { if $0.description == card.description { count = count + 1 } }
            if count == matchSize {
                return true
            }
        }
        
        return false
    }
    
    private func getCardCounts(hand: Hand) -> [String:Int] {
        var counts = [String:Int]()
        for card in hand {
            if counts.keys.contains(card.description) {
                counts[card.description] = 0
            }
            
            hand.forEach {
                if $0.description == card.description {
                    let previousCount = counts[card.description] ?? 0
                    counts[card.description] = previousCount + 1
                }
            }
        }
        
        return counts
    }
    
    private func getBestMatchHand() -> Hand? {
        let hand1Counts = getCardCounts(hand: hand1).filter { $0.value > 1 }
        let sortedHand1 = hand1.sorted { card1, card2 -> Bool in
            if (hand1Counts.keys.contains(card1.description) && hand1Counts.keys.contains(card2.description)) ||
                (!hand1Counts.keys.contains(card1.description) && !hand1Counts.keys.contains(card2.description))
            {
                return card1.intrinsicValue > card2.intrinsicValue
            } else if hand1Counts.keys.contains(card1.description) {
                return true
            }
            
            return false
        }
        
        let hand2Counts = getCardCounts(hand: hand2).filter { $0.value > 1 }
        let sortedHand2 = hand2.sorted { card1, card2 -> Bool in
            if (hand2Counts.keys.contains(card1.description) && hand2Counts.keys.contains(card2.description)) ||
                (!hand2Counts.keys.contains(card1.description) && !hand2Counts.keys.contains(card2.description))
            {
                return card1.intrinsicValue > card2.intrinsicValue
            } else if hand2Counts.keys.contains(card1.description) {
                return true
            }
            
            return false
        }
        
        for i in 0...4 {
            if sortedHand1[i].intrinsicValue > sortedHand2[i].intrinsicValue {
                return hand1
            } else if sortedHand1[i].intrinsicValue < sortedHand2[i].intrinsicValue {
                return hand2
            }
        }
        
        return nil
    }
    
    private func getHandWithHighestCard() -> Hand? {
        let sortedHand1 = hand1.sorted(by: cardValueSorter)
        let sortedHand2 = hand2.sorted(by: cardValueSorter)
        
        for i in 0...4 {
            if sortedHand1[i].intrinsicValue > sortedHand2[i].intrinsicValue {
                return hand1
            } else if sortedHand1[i].intrinsicValue < sortedHand2[i].intrinsicValue {
                return hand2
            }
        }
        
        return nil
    }
}

// FOUR-OF-A-KIND EVALUATION
extension PokerHandEvaluator {
    func hasFourOfAKind(hand: Hand) -> Bool {
        let foundFour = hasMatch(hand: hand, matchSize: 4)
        
        // if we didn't find four-of-a-kind, do we have three-of-a-kind and a wildcard?
        if !foundFour && hand.contains(Card.wild) && hasMatch(hand: hand, matchSize: 3) {
            return true
        }
        
        return foundFour
    }
}

// THREE-OF-A-KIND EVALUATION
extension PokerHandEvaluator {
    func hasThreeOfAKind(hand: Hand) -> Bool {
        let foundThree = hasMatch(hand: hand, matchSize: 3)
        
        // if we didn't find three-of-a-kind, see if we have a pair and a wildcard
        if !foundThree && hand.contains(.wild) && hasMatch(hand: hand, matchSize: 2) {
            return true
        }
        
        return foundThree
    }
}

// PAIR EVALUATION
extension PokerHandEvaluator {
    func hasPair(hand: Hand) -> Bool {
        let foundPair = hasMatch(hand: hand, matchSize: 2)
        
        // if we don't have a pair, but we have a wild, then we can create a pair
        if !foundPair && hand.contains(.wild) {
            return true
        }
        
        return foundPair
    }
}

// TWO-PAIR EVALUATION
extension PokerHandEvaluator {
    func hasTwoPair(hand: Hand) -> Bool {
        let cardCounts = getCardCounts(hand: hand)
        return cardCounts.values.filter { $0 == 2 }.count == 2
    }
}

// FULL-HOUSE EVALUATION
extension PokerHandEvaluator {
    func hasFullHouse(hand: Hand) -> Bool {
        var counts = [String:Int]()
        for card in hand {
            if counts.keys.contains(card.description) {
                counts[card.description] = 0
            }
            
            hand.forEach {
                if $0.description == card.description {
                    let previousCount = counts[card.description] ?? 0
                    counts[card.description] = previousCount + 1
                }
            }
        }
        
        let foundFullHouse = counts.values.contains(3) && counts.values.contains(2)
        
        // if we didn't find a full-house, do we have two pairs and a wildcard?
        if !foundFullHouse && hand.contains(.wild) && (counts.values.filter { $0 == 2 }.count == 2) {
            return true
        }
        
        return foundFullHouse
    }
    
    func getBestFullHouse() -> Hand? {
        let sortedHand1 = hand1.sorted(by: cardValueSorter)
        let sortedHand2 = hand2.sorted(by: cardValueSorter)
        
        if sortedHand1 == sortedHand2 {
            return nil
        }
        
        if sortedHand1.first!.intrinsicValue == sortedHand2.first!.intrinsicValue {
            return sortedHand1[3].intrinsicValue > sortedHand2[3].intrinsicValue ? sortedHand1 : sortedHand2
        }
        
        return sortedHand1.first!.intrinsicValue > sortedHand2.first!.intrinsicValue ? hand1 : hand2
    }
}

// STRAIGHT EVALUATION
extension PokerHandEvaluator {
    func hasStraight(hand: Hand) -> Bool {
        var foundStraight = true
        
        let sortedHand = hand.sorted(by: cardStraightSorter)
        for i in 1...4 {
            if sortedHand[i].straightValue != sortedHand[i-1].straightValue + 1 {
                foundStraight = false
                break
            }
        }
        
        // if we didn't find a straight, do we have a wildcard? if so, can we create a straight?
        if !foundStraight && hand.contains(.wild) && createStraight(hand: hand) != nil {
            return true
        }
        
        return foundStraight
    }
    
    func getBestStraight() -> Hand? {
        let sortedHand1 = hand1.contains(.wild) ? createStraight(hand: hand1)!.sorted(by: cardStraightSorter) : hand1.sorted(by: cardStraightSorter)
        let sortedHand2 = hand2.contains(.wild) ? createStraight(hand: hand2)!.sorted(by: cardStraightSorter) : hand2.sorted(by: cardStraightSorter)
        
        if (sortedHand1 == sortedHand2) {
            return nil
        }
        
        return sortedHand1.last!.straightValue > sortedHand2.last!.straightValue ? hand1 : hand2
    }
    
    // will attempt to create a straight if the hand contains a wild card
    func createStraight(hand: Hand) -> Hand? {
        if !hand.contains(.wild) {
            return nil
        }
        
        let allCards = (Card.validCards.filter { $0 != .wild }).sorted(by: cardStraightSorter)
        for card in allCards.reversed() {
            var tempHand = hand
            tempHand.remove(at: hand.index(of: .wild)!)
            tempHand.append(card)
            if (hasStraight(hand: tempHand)) {
                return tempHand
            }
        }
        
        return nil
    }
}
