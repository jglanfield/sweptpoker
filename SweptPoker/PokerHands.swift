//
//  PokerHands.swift
//  SweptPoker
//
//  Created by Joel Glanfield on 2018-06-28.
//  Copyright © 2018 Joel Glanfield. All rights reserved.
//

import Foundation

typealias Hand = [Card]

// Note: I'm using a Swift enum to show the power of associated values and pattern-matching.
enum Card: Equatable {
    case ace
    case king
    case queen
    case jack
    case ten
    case number(Int)
    case wild
    case unknown
    
    var description: String {
        switch self {
        case .ace:
            return "A"
        case .king:
            return "K"
        case .queen:
            return "Q"
        case .jack:
            return "J"
        case .ten:
            return "T"
        case .number(let value):
            return String(value)
        case .wild:
            return "*"
        case .unknown:
            return "?"
        }
    }
    
    var intrinsicValue: Int {
        switch self {
        case .ace:
            return 14
        case .king:
            return 13
        case .queen:
            return 12
        case .jack:
            return 11
        case .ten:
            return 10
        case .number(let value):
            return value
        case .wild:
            return 100
        case .unknown:
            return -1
        }
    }
    
    // Assumption: in a straight, an Ace is the lowest card possible
    var straightValue: Int {
        switch self {
        case .ace:
            return 1
        case .king:
            return 13
        case .queen:
            return 12
        case .jack:
            return 11
        case .ten:
            return 10
        case .number(let value):
            return value
        case .wild:
            return 100
        case .unknown:
            return -1
        }
    }
    
    static var validCards: [Card] {
        var cards = [Card.ace, Card.king, Card.queen, Card.jack, Card.ten, Card.wild]
        cards.append(contentsOf: Array(2...9).map { Card.number($0) })
        
        return cards
    }
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.description == rhs.description
    }
}

class PokerHands {
    func isValidHands() -> (hand1: Hand, hand2: Hand)? {
        let argCount = CommandLine.argc
        if (argCount < 4) {
            log("Missing hands. Try -h for help.")
            exit(0)
        }
        
        let hand1 = convertToHand(inputHand: CommandLine.arguments[2])
        let hand2 = convertToHand(inputHand: CommandLine.arguments[3])
        
        if let cardFrequency = hand1.isValidCardFrequencies(other: hand2) {
            log("Too many occurences of: \(cardFrequency.card) (\(cardFrequency.frequency))")
            return nil
        }

        if !hand1.isValidPokerHand() {
            log("Invalid poker hand: \(handDescription(for: hand1))")
            return nil
        }

        if !hand2.isValidPokerHand() {
            log("Invalid poker hand: \(handDescription(for: hand2))")
            return nil
        }
        
        return (hand1, hand2)
    }
    
    private func convertToHand(inputHand: String) -> Hand {
        let validCards = Card.validCards
        
        let hand: [Card] = inputHand.map { cardString in
            let cardsFound = validCards.filter { $0.description == String(cardString) }
            if (cardsFound.isEmpty) {
                return Card.unknown
            } else {
                return cardsFound.first!
            }
        }
        
        return hand
    }
}

extension Array where Element == Card {
    
    // Assumptions:
    // - wild card can only show up once per hand
    // - we can have up to 8 of any one card (except wilds) since we are essentially using two decks
    func isValidPokerHand() -> Bool {
        return self.filter { card -> Bool in return Card.validCards.contains { $0.description == card.description } }.count == 5 // hand should contain 5 valid cards
            && self.filter { card -> Bool in card.description == "*"}.count <= 1 // hand should have at most 1 wildcard
    }
    
    func isValidCardFrequencies(other: Hand) -> (card: String, frequency: Int)? {
        var invalidCard: (String, Int)?

        let combined = "\(handDescription(for: self))\(handDescription(for: other))"
        for card in Card.validCards {
            let occurences = combined.filter { String($0) == card.description }.count
            if occurences > 8 { // no card should appear more than 8 times across both hands
                invalidCard = (card.description, occurences)
                break
            }
        }

        return invalidCard
    }
}

func handDescription(for hand: Hand) -> String {
    return String(hand.flatMap { $0.description })
}

func cardValueSorter(card1: Card, card2: Card) -> Bool {
    return card1.intrinsicValue > card2.intrinsicValue
}

func cardStraightSorter(card1: Card, card2: Card) -> Bool {
    return card1.straightValue < card2.straightValue
}

func handValue(for hand: Hand) -> Int {
    return hand.reduce(0) { result, card -> Int in
        return card.intrinsicValue + result
    }
}
