//
//  main.swift
//  SweptPoker
//
//  Created by Joel Glanfield on 2018-06-28.
//  Copyright © 2018 Joel Glanfield. All rights reserved.
//

import Foundation

enum ProgramOption: String {
    case play = "-p"
    case usage = "-h"
    case unknown
    
    init(value: String) {
        switch (value) {
        case "-h": self = .usage
        case "-p": self = .play
        default: self = .unknown
        }
    }
    
    static func getOption(_ option: String) -> ProgramOption {
        return ProgramOption(value: option)
    }
}

func printUsage() {
    let executable = (CommandLine.arguments[0] as NSString).lastPathComponent
    
    log("Usage: \(executable) -p <hand1> <hand2>")
    log("Example: \(executable) -p AA222 KKK55")
}

let pokerHands = PokerHands()

let argCount = CommandLine.argc
if (argCount < 2) {
    log("Invalid program execution. Try -h for help.")
    exit(0)
}

let arg = CommandLine.arguments[1]
let option = ProgramOption.getOption(arg)

switch (option) {
    case .usage: printUsage()
    case .play:
        if let hands = pokerHands.isValidHands() {
            log("\(handDescription(for: hands.hand1)) vs \(handDescription(for: hands.hand2))")
            let evaluator = PokerHandEvaluator(hand1: hands.hand1, hand2: hands.hand2)
            guard let results = evaluator.evaluate() else {
                log(" -> no winning hand")
                break
            }

            if (results.hand == hands.hand1) {
                log(" -> \(handDescription(for: hands.hand1)) \(results.winnerDescription) > \(handDescription(for: hands.hand2)) \(results.loserDescription)")
            } else {
                log(" -> \(handDescription(for: hands.hand1)) \(results.loserDescription) < \(handDescription(for: hands.hand2)) \(results.winnerDescription)")
            }
        } else {
            exit(0)
        }
    case .unknown: log("Invalid argument. Try -h for help.")
}
