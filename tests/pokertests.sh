#!/bin/sh

testhands=(
	'23333' '44544'
	'23455' 'AKQQJ'
	'KKKAA' '44442'
	'Qbcde' '22223'
	'2244Q' '44K22'
	'2244Q' '44Q22'
	'2244*' '3*456'
	'357T*' '23457'
	'445*2' '445*4'
)

for (( i=0; i<${#testhands[@]}; i+=2 ));
do
    hand1=${testhands[i]}
    hand2=${testhands[i+1]}

    ./SweptPoker -p $hand1 $hand2
done
